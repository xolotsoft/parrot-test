# Command Line Commands

Use [YARN](https://yarnpkg.com/getting-started/install) as package manager

## Initialization

```Shell
yarn start
```

Starts the development server of application running on `http://localhost:8010`

Starts the development server of landing page running on `http://localhost:3000`

## Rebuilding Componets

```Shell
lerna run build:lib
```