import { Header, Main, Section, Footer } from "@parrot/components";
import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <div>
      <Header />
      <Main />
      <Section />
      <Footer />
    </div>
  );
}
