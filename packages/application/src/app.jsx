import React from 'react'
import { render } from 'react-dom'

import { Root } from './root'

const root = document.getElementById('main')

render(<Root />, root)
