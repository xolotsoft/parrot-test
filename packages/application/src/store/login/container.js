import { connect } from 'react-redux'
import { bindActionCreators, compose } from 'redux'

import { loginBindActions } from './actions'
import * as selectors from './selectors'
import { Login as Component } from '../../components/login'

const mapStateToProps = state => ({
    username: selectors.usernameSelector(state),
    password: selectors.passwordSelector(state),
    isConsulting: selectors.isConsultingSelector(state),
    message: selectors.messageSelector(state)
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(loginBindActions, dispatch)
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export const Login = compose(withConnect)(Component)
