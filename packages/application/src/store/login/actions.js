import * as constants from './constants'

const handleUsernameAction = payload => ({
    type: constants.HANDLE_LOGIN_USERNAME_ACTION,
    payload
})
const handlePasswordAction = payload => ({
    type: constants.HANDLE_LOGIN_PASSWORD_ACTION,
    payload
})
const handleSubmitAction = () => ({
    type: constants.HANDLE_LOGIN_SUBMIT_ACTION
})
const handleSubmitStarted = () => ({
    type: constants.HANDLE_LOGIN_SUBMIT_STARTED
})
const handleSubmitFinished = () => ({
    type: constants.HANDLE_LOGIN_SUBMIT_FINISHED
})
const handleSubmitError = payload => ({
    type: constants.HANDLE_LOGIN_SUBMIT_ERROR,
    payload
})
const handleValidateAction = () => ({
    type: constants.HANDLE_LOGIN_VALIDATE_ACTION
})
const handleRefreshAction = () => ({
    type: constants.HANDLE_LOGIN_REFRESH_ACTION
})

const handleUsernameReducer = payload => ({
    type: constants.HANDLE_LOGIN_USERNAME_REDUCER,
    payload
})
const handlePasswordReducer = payload => ({
    type: constants.HANDLE_LOGIN_PASSWORD_REDUCER,
    payload
})
const handleAccessReducer = payload => ({
    type: constants.HANDLE_LOGIN_ACCESS_REDUCER,
    payload
})
const handleRefreshReducer = payload => ({
    type: constants.HANDLE_LOGIN_REFRESH_REDUCER,
    payload
})
const handleIsConsultingReducer = payload => ({
    type: constants.HANDLE_LOGIN_IS_CONSULTING_REDUCER,
    payload
})
const handleIsLoggedReducer = payload => ({
    type: constants.HANDLE_LOGIN_IS_LOGGED_REDUCER,
    payload
})
const handleMessageReducer = payload => ({
    type: constants.HANDLE_LOGIN_MESSAGE_REDUCER,
    payload
})

export const loginBindActions = {
    handleUsernameAction,
    handlePasswordAction,
    handleSubmitAction
}
export const loginSagaActions = {
    handleUsernameReducer,
    handlePasswordReducer,
    handleAccessReducer,
    handleRefreshReducer,
    handleIsConsultingReducer,
    handleValidateAction,
    handleRefreshAction,
    handleSubmitStarted,
    handleSubmitFinished,
    handleSubmitError,
    handleIsLoggedReducer,
    handleMessageReducer
}
