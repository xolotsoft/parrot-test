/* eslint-disable import/no-cycle */
import { put, takeEvery, select, call } from 'redux-saga/effects'

import { history } from '../../utils'
import { loginSagaActions as actions } from './actions'
import * as constants from './constants'
import * as selectors from './selectors'

const { BASE_URL } = process.env

function* handleUsernameHandler(action) {
    try {
        const { payload } = action
        yield put(actions.handleUsernameReducer(payload))
    } catch (error) {
        // console.log(error)
    }
}

function* handlePasswordHandler(action) {
    try {
        const { payload } = action
        yield put(actions.handlePasswordReducer(payload))
    } catch (error) {
        // console.log(error)
    }
}

function* validateTokenHandler() {
    try {
        yield put(actions.handleIsLoggedReducer(false))
        const access = yield select(selectors.accessSelector)
        const response = yield fetch(`${BASE_URL}/api/auth/token/test`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${access}`
            }
        })
        const body = yield response.json()
        if (body.status === 'ok') {
            yield put(actions.handleIsLoggedReducer(true))
        } else {
            yield call(history.push, '/')
        }
    } catch (error) {
        yield put(actions.handleIsLoggedReducer(false))
    }
}

function* refreshTokenHandler() {
    try {
        const refresh = yield select(selectors.refreshSelector)
        const response = yield fetch(`${BASE_URL}/api/auth/token/refresh`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ refresh })
        })
        const body = yield response.json()
        yield put(actions.handleAccessReducer(body.access))
        yield put(actions.handleRefreshReducer(body.refresh))
    } catch (error) {
        // console.log(error)
    }
}

function* handleSubmitHandler() {
    try {
        yield put(actions.handleSubmitStarted())
        yield put(actions.handleIsConsultingReducer(true))
        const username = yield select(selectors.usernameSelector)
        const password = yield select(selectors.passwordSelector)
        const response = yield fetch(`${BASE_URL}/api/auth/token`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ username, password })
        })
        if (response.status > 299) {
            throw new Error('problemas')
        }
        const body = yield response.json()
        yield put(actions.handleAccessReducer(body.access))
        yield put(actions.handleRefreshReducer(body.refresh))
        yield localStorage.setItem('accessToken', body.access)
        yield localStorage.setItem('refreshToken', body.refresh)
        yield put(actions.handleIsLoggedReducer(true))
        yield put(actions.handleUsernameReducer(''))
        yield put(actions.handlePasswordReducer(''))
        yield put(actions.handleMessageReducer(''))
        yield put(actions.handleIsConsultingReducer(false))
        yield put(actions.handleSubmitFinished())
        yield call(history.push, 'menu')
    } catch (error) {
        yield put(actions.handleIsConsultingReducer(false))
        yield put(
            actions.handleMessageReducer(
                'Verifica que los datos sean correctos.'
            )
        )
        yield put(actions.handleSubmitError(error))
    }
}

export function* saga() {
    yield takeEvery(
        constants.HANDLE_LOGIN_USERNAME_ACTION,
        handleUsernameHandler
    )
    yield takeEvery(
        constants.HANDLE_LOGIN_PASSWORD_ACTION,
        handlePasswordHandler
    )
    yield takeEvery(
        constants.HANDLE_LOGIN_VALIDATE_ACTION,
        validateTokenHandler
    )
    yield takeEvery(constants.HANDLE_LOGIN_SUBMIT_ACTION, handleSubmitHandler)
    yield takeEvery(constants.HANDLE_LOGIN_REFRESH_ACTION, refreshTokenHandler)
}
