import produce from 'immer'
import { handleActions } from 'redux-actions'

import { state as initialState } from './state'
import * as constants from './constants'

const handleUsername = (state, action) => {
    const { payload } = action
    return produce(state, draft => {
        draft.username = payload
    })
}
const handlePassword = (state, action) => {
    const { payload } = action
    return produce(state, draft => {
        draft.password = payload
    })
}
const handleAccess = (state, action) => {
    const { payload } = action
    return produce(state, draft => {
        draft.access = payload
    })
}
const handleRefresh = (state, action) => {
    const { payload } = action
    return produce(state, draft => {
        draft.refresh = payload
    })
}
const handleIsConsulting = (state, action) => {
    const { payload } = action
    return produce(state, draft => {
        draft.isConsulting = payload
    })
}
const handleIsLogged = (state, action) => {
    const { payload } = action
    return produce(state, draft => {
        draft.isLogged = payload
    })
}
const handleMessage = (state, action) => {
    const { payload } = action
    return produce(state, draft => {
        draft.message = payload
    })
}

const reducer = produce(
    handleActions(
        {
            [constants.HANDLE_LOGIN_USERNAME_REDUCER]: handleUsername,
            [constants.HANDLE_LOGIN_PASSWORD_REDUCER]: handlePassword,
            [constants.HANDLE_LOGIN_ACCESS_REDUCER]: handleAccess,
            [constants.HANDLE_LOGIN_REFRESH_REDUCER]: handleRefresh,
            [constants.HANDLE_LOGIN_IS_CONSULTING_REDUCER]: handleIsConsulting,
            [constants.HANDLE_LOGIN_IS_LOGGED_REDUCER]: handleIsLogged,
            [constants.HANDLE_LOGIN_MESSAGE_REDUCER]: handleMessage
        },
        initialState
    )
)

export { reducer }
