export const HANDLE_LOGIN_USERNAME_REDUCER = 'login/handleUsernameReducer'
export const HANDLE_LOGIN_PASSWORD_REDUCER = 'login/handlePasswordReducer'
export const HANDLE_LOGIN_ACCESS_REDUCER = 'login/handleAccessReducer'
export const HANDLE_LOGIN_REFRESH_REDUCER = 'login/handleRefreshdReducer'
export const HANDLE_LOGIN_IS_CONSULTING_REDUCER =
    'login/handleIsConsultingdReducer'
export const HANDLE_LOGIN_IS_LOGGED_REDUCER = 'login/handleIsLoggedReducer'
export const HANDLE_LOGIN_MESSAGE_REDUCER = 'login/handleMessageReducer'

export const HANDLE_LOGIN_USERNAME_ACTION = 'login/handleUsernameAction'
export const HANDLE_LOGIN_PASSWORD_ACTION = 'login/handlePasswordAction'

export const HANDLE_LOGIN_SUBMIT_ACTION = 'login/handleSubmitAction'
export const HANDLE_LOGIN_SUBMIT_STARTED = 'login/handleSubmitStarted'
export const HANDLE_LOGIN_SUBMIT_FINISHED = 'login/handleSubmitFinished'
export const HANDLE_LOGIN_SUBMIT_ERROR = 'login/handleSubmitError'

export const HANDLE_LOGIN_VALIDATE_ACTION = 'login/handleValidateAction'
export const HANDLE_LOGIN_REFRESH_ACTION = 'login/handleRefreshAction'
