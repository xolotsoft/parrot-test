import _ from 'lodash'
import { createSelector } from 'reselect'

import { state as initialState } from './state'

const loginSelector = state => _.get(state, 'login', initialState)

export const usernameSelector = createSelector(loginSelector, state =>
    _.get(state, ['username'])
)
export const passwordSelector = createSelector(loginSelector, state =>
    _.get(state, ['password'])
)
export const accessSelector = createSelector(loginSelector, state =>
    _.get(state, ['access'])
)
export const refreshSelector = createSelector(loginSelector, state =>
    _.get(state, ['refresh'])
)
export const isConsultingSelector = createSelector(loginSelector, state =>
    _.get(state, ['isConsulting'])
)
export const isLoggedSelector = createSelector(loginSelector, state =>
    _.get(state, ['isLogged'])
)
export const messageSelector = createSelector(loginSelector, state =>
    _.get(state, ['message'])
)
