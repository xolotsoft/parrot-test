import produce from 'immer'
import { handleActions } from 'redux-actions'

import { state as initialState } from './state'
import * as constants from './constants'

const handleData = (state, action) => {
    const { payload } = action
    return produce(state, draft => {
        draft.data = payload
    })
}

const reducer = produce(
    handleActions(
        {
            [constants.HANDLE_MENU_DATA_REDUCER]: handleData
        },
        initialState
    )
)

export { reducer }
