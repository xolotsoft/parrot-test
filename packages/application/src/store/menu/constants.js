export const HANDLE_MENU_DATA_REDUCER = 'menu/handleDataReducer'

export const HANDLE_MENU_GET_DATA_STARTED = 'menu/handleGetDataStarted'
export const HANDLE_MENU_GET_DATA_FINISHED = 'menu/handleGetDataFinished'
export const HANDLE_MENU_GET_DATA_ERROR = 'menu/handleGetDataError'

export const HANDLE_MENU_UPDATE_DATA = 'menu/handleUpdateData'
