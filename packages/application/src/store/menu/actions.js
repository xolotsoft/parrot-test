import * as constants from './constants'

const handleDataReducer = payload => ({
    type: constants.HANDLE_MENU_DATA_REDUCER,
    payload
})

const handleGetDataStarted = () => ({
    type: constants.HANDLE_MENU_GET_DATA_FINISHED
})
const handleGetDataFinished = () => ({
    type: constants.HANDLE_MENU_GET_DATA_FINISHED
})
const handleGetDataError = payload => ({
    type: constants.HANDLE_MENU_GET_DATA_ERROR,
    payload
})
const handleUpdateDataAction = payload => ({
    type: constants.HANDLE_MENU_UPDATE_DATA,
    payload
})

export const loginBindActions = {
    handleUpdateDataAction
}
export const loginSagaActions = {
    handleDataReducer,
    handleGetDataStarted,
    handleGetDataFinished,
    handleGetDataError
}
