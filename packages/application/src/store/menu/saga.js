import { put, call, select, takeLatest } from 'redux-saga/effects'

import { loginSagaActions as actions } from './actions'
import { loginSagaActions as loginActions } from '../login/actions'
import * as loginSelectors from '../login/selectors'
import { history } from '../../utils'
import * as constants from './constants'

const { BASE_URL } = process.env
const { STORE_ID } = process.env

function* getData(access) {
    const response = yield fetch(
        `${BASE_URL}/api/v1/products/?store=${STORE_ID}`,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${access}`
            }
        }
    )
    const body = yield response.json()
    const products = body.results
    const filtered = products.filter(
        (v, i, a) => a.findIndex(t => t.category.uuid === v.category.uuid) === i
    )
    const categories = []
    for (let index = 0; index < filtered.length; index++) {
        categories.push({ ...filtered[index].category, products: [] })
    }
    for (let index = 0; index < products.length; index++) {
        for (let i = 0; i < categories.length; i++) {
            if (products[index].category.uuid === categories[i].uuid) {
                categories[i].products.push(products[index])
            }
        }
    }
    yield put(actions.handleDataReducer(categories))
}
function* updateData({ access, id, availability }) {
    const value = availability === 'AVAILABLE' ? 'UNAVAILABLE' : 'AVAILABLE'
    const response = yield fetch(
        `${BASE_URL}/api/v1/products/${id}/availability`,
        {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${access}`
            },
            body: JSON.stringify({ availability: value })
        }
    )
    return response
}
function* updateDataHandler(action) {
    try {
        yield put(actions.handleGetDataStarted())
        const { id, availability } = action.payload
        const isLogged = yield select(loginSelectors.isLoggedSelector)
        if (isLogged) {
            const access = yield localStorage.getItem('accessToken')
            yield call(updateData, { access, id, availability })
            yield call(getData, access)
        } else {
            yield call(history.push, '/')
        }
        yield put(actions.handleGetDataFinished())
    } catch (error) {
        yield put(actions.handleGetDataError(error))
    }
}
function* getDataHandler() {
    try {
        yield put(actions.handleGetDataStarted())
        const access = yield localStorage.getItem('accessToken')
        const refresh = yield localStorage.getItem('refreshToken')
        if (access) {
            yield put(loginActions.handleAccessReducer(access))
            yield put(loginActions.handleRefreshReducer(refresh))
            yield put(loginActions.handleValidateAction())
            yield call(getData, access)
        } else {
            throw new Error('No se encontro el token de acceso.')
        }
        yield put(actions.handleGetDataFinished())
    } catch (error) {
        yield call(history.push, '/')
        yield put(actions.handleGetDataError(error))
    }
}

export function* saga() {
    yield call(getDataHandler)
    yield takeLatest(constants.HANDLE_MENU_UPDATE_DATA, updateDataHandler)
}
