import { connect } from 'react-redux'
import { bindActionCreators, compose } from 'redux'

import { injectReducer, injectSaga } from '../../components/utils'
import { loginBindActions } from './actions'
import * as selectors from './selectors'
import { reducer } from './reducer'
import { saga } from './saga'
import { Menu as Component } from '../../components/menu'

const mapStateToProps = state => ({
    data: selectors.dataSelector(state)
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(loginBindActions, dispatch)
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

const withReducer = injectReducer('menu', reducer)

const withSaga = injectSaga('menu', saga)

export const Menu = compose(withReducer, withSaga, withConnect)(Component)
