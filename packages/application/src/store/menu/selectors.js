import _ from 'lodash'
import { createSelector } from 'reselect'

import { state as initialState } from './state'

const menuSelector = state => _.get(state, 'menu', initialState)

export const dataSelector = createSelector(menuSelector, state =>
    _.get(state, ['data'])
)
