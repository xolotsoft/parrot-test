import { createStore, applyMiddleware, compose } from 'redux'
import { routerMiddleware } from 'connected-react-router'
import createSagaMiddleware from 'redux-saga'

import { createReducer } from './create-reducer'
import { history } from './history'
import { saga } from '../store/login/saga'

const sagaMiddleware = createSagaMiddleware({
    onError(error) {
        setImmediate(() => {
            throw error
        })
    }
})

const middlewares = [sagaMiddleware, routerMiddleware(history)]
const enhancers = [applyMiddleware(...middlewares)]

const composeEnhancers =
    process.env.NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
        ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
        : compose

const initializeStore = () => {
    const store = createStore(createReducer(), composeEnhancers(...enhancers))
    sagaMiddleware.run(saga)
    store.asyncReducers = {}
    store.asyncSagas = {}
    store.injectSaga = (key, asyncSaga) => {
        if (key in store.asyncSagas) return
        const task = sagaMiddleware.run(asyncSaga)
        store.asyncSagas[key] = task
    }
    store.injectReducer = (key, asyncReducer) => {
        store.asyncReducers[key] = asyncReducer
        store.replaceReducer(
            createReducer(store.asyncReducers),
            composeEnhancers(...enhancers)
        )
        return store
    }

    return store
}

export { initializeStore }
