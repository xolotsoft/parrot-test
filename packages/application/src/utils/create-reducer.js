import { connectRouter } from 'connected-react-router'
import { combineReducers } from 'redux'

import { history } from './history'
import { reducer } from '../store/login/reducer'

const staticReducers = {
    router: connectRouter(history),
    login: reducer
}

const createReducer = asyncReducers =>
    combineReducers({ ...staticReducers, ...asyncReducers })

export { createReducer }
