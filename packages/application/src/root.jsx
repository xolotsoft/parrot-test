import React from 'react'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { Switch, Route } from 'react-router-dom'
import 'regenerator-runtime/runtime'
import './styles/style.scss'

import { initializeStore, history } from './utils'

import { Login, Menu } from './store'

const Root = () => (
    <Provider store={initializeStore()}>
        <ConnectedRouter history={history}>
            <Switch>
                <Route path="/" exact component={Login} />
                <Route path="/menu" component={Menu} />
            </Switch>
        </ConnectedRouter>
    </Provider>
)

export { Root }
