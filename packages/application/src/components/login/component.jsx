import React from 'react'
import p from 'prop-types'

const Login = ({ username, password, isConsulting, message, actions }) => (
    <div className="login">
        <div className="container">
            <div className="login-panel">
                <div className="logo" />
                <h3>¡bienvenido!</h3>
                <label htmlFor="username">Usuario</label>
                <input
                    type="text"
                    id="username"
                    value={username}
                    onChange={e => actions.handleUsernameAction(e.target.value)}
                />
                <br />
                <label htmlFor="username">Contraseña</label>
                <input
                    type="password"
                    id="password"
                    value={password}
                    onChange={e => actions.handlePasswordAction(e.target.value)}
                />
                <p>{message}</p>
                <button
                    type="button"
                    onClick={actions.handleSubmitAction}
                    disabled={isConsulting}
                >
                    Entrar
                </button>
            </div>
        </div>
    </div>
)

Login.propTypes = {
    username: p.string.isRequired,
    password: p.string.isRequired,
    isConsulting: p.bool.isRequired,
    message: p.string.isRequired,
    actions: p.shape({
        handleUsernameAction: p.func,
        handlePasswordAction: p.func,
        handleSubmitAction: p.func
    }).isRequired
}

export { Login }
