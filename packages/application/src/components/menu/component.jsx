/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React from 'react'
import p from 'prop-types'
import _ from 'lodash'

const Menu = ({ data, actions }) => (
    <div className="menu">
        <ul>
            {data.map(e => (
                <li key={_.uniqueId('çategory-')}>
                    <p>{`${e.name} (${e.products.length})`}</p>
                    <ul>
                        {e.products.map(el => (
                            <li
                                key={_.uniqueId('product-')}
                                onClick={() =>
                                    actions.handleUpdateDataAction({
                                        id: el.uuid,
                                        availability: el.availability
                                    })
                                }
                            >
                                <img src={el.imageUrl} alt={el.name} />
                                <p>
                                    {el.name} <br />$ {el.price}
                                </p>
                                <input
                                    type="checkbox"
                                    checked={el.availability === 'AVAILABLE'}
                                />
                            </li>
                        ))}
                    </ul>
                </li>
            ))}
        </ul>
    </div>
)

Menu.propTypes = {
    actions: p.objectOf(
        p.shape({
            handleUpdateDataAction: p.func
        })
    ).isRequired,
    data: p.arrayOf(
        p.shape({
            uuid: p.string,
            name: p.string,
            sortPosition: p.number,
            products: p.arrayOf(
                p.shape({
                    uuid: p.string,
                    name: p.string,
                    description: p.string,
                    imageUrl: p.string,
                    legacyId: p.number,
                    price: p.number,
                    alcoholCount: p.number,
                    soldAlone: p.bool,
                    availability: p.string,
                    providerAvailability: p.string
                })
            )
        })
    ).isRequired
}

export { Menu }
