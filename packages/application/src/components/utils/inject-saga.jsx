import React from 'react'
import { ReactReduxContext } from 'react-redux'

const injectSaga = (key, saga) => WrappedComponent => {
    const Extended = props => {
        const context = React.useContext(ReactReduxContext)
        const { store } = context
        store.injectSaga(key, saga)
        return <WrappedComponent {...props} />
    }
    return Extended
}

export { injectSaga }
