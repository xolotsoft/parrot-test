import React from 'react'
import { ReactReduxContext } from 'react-redux'

const injectReducer = (key, reducer) => WrappedComponent => {
    const Extended = props => {
        const context = React.useContext(ReactReduxContext)
        const { store } = context
        store.injectReducer(key, reducer)
        return <WrappedComponent {...props} />
    }
    return Extended
}

export { injectReducer }
