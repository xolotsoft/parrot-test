import React, { Suspense, lazy } from 'react'

const loadable = Component => {
    const LazyComponent = lazy(Component)
    return props => (
        <Suspense fallback={<div>Loading...</div>}>
            <LazyComponent {...props} />
        </Suspense>
    )
}

export { loadable }
