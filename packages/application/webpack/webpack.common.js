const HtmlWebPackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
// const CopyPlugin = require('copy-webpack-plugin')
const webpack = require('webpack')
const dotenv = require('dotenv')
const path = require('path')

const env = dotenv.config().parsed
const envKeys =
    env !== undefined &&
    Object.keys(env).reduce((prev, next) => {
        const previous = prev
        previous[`process.env.${next}`] = JSON.stringify(env[next])
        return previous
    }, {})

module.exports = () => ({
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.scss$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin(envKeys),
        new MiniCssExtractPlugin({
            filename: 'css/style.css'
        }),
        new HtmlWebPackPlugin({
            hash: true,
            inject: true,
            template: path.join(process.cwd(), 'public/index.html')
        })
    ]
})
