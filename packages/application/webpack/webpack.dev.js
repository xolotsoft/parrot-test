const path = require('path')
const { merge } = require('webpack-merge')
const common = require('./webpack.common')

module.exports = env =>
    merge(common(env), {
        mode: 'development',
        devtool: 'source-map',
        devServer: {
            historyApiFallback: true,
            hot: true,
            open: true,
            port: 8010,
            injectClient: true,
            clientLogLevel: 'none',
            compress: true,
            quiet: true,
            overlay: false
        },
        entry: {
            app: path.join(process.cwd(), 'src/app.jsx')
        },
        output: {
            path: path.join(process.cwd(), 'dist'),
            pathinfo: true,
            filename: 'js/bundle.js',
            publicPath: '/'
        }
    })
