import styled from 'styled-components'

import { Footer } from './component'

const FooterStyled = styled(Footer)`
    font-family: Arial, Helvetica, sans-serif;
    background: #1d264c;
    width: 100%;
    min-height: 400px;
    display: flex;
    .container {
        max-width: 1140px;
        width: 100%;
        margin: 0 auto;
        padding: 32px 0;
        p {
            color: #fff;
            text-align: center;
            font-size: 1rem;
            width: 80%;
            margin: 0 auto;
            display: block;
            font-weight: lighter;
        }
        .menu-container {
            display: flex;
            align-items: flex-start;
            justify-content: center;
            flex-wrap: wrap;
            .col {
                padding: 8px;
                h3 {
                    color: #59bfa0;
                    font-size: 20px;
                    margin-top: 40px;
                }
                ul {
                    padding: 0;
                    list-style: none;
                    li {
                        font-size: 14px;
                        color: #ffffff;
                        padding: 4px 0;
                    }
                }
            }
        }
    }
`

export { FooterStyled as Footer }
