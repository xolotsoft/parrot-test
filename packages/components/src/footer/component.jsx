import React from 'react'
import p from 'prop-types'

const Footer = ({ className }) => (
    <div className={className}>
        <div className="container">
            <p>
                Todas las marcas o derechos de cualquier tipo, nombres
                registrados, logos o insignias, usados o citados en esta web,
                son propiedad de sus respectivos dueños.
            </p>
            <div className="menu-container">
                <div className="col">
                    <h3>Parrot Software</h3>
                    <ul>
                        <li>Nosotros</li>
                        <li>Inicia Sesión</li>
                        <li>Solicita un Demo</li>
                    </ul>
                    <h3>Redes Sociales</h3>
                </div>
                <div className="col">
                    <h3>Software</h3>
                    <ul>
                        <li>App</li>
                        <li>Delivery Service</li>
                        <li>Inventarios</li>
                        <li>Kitchen Display System (KDS)</li>
                        <li>Multisucursal</li>
                        <li>ParrotPay</li>
                        <li>Portales Web</li>
                        <li>Punto de venta</li>
                        <li>Reloj Checador</li>
                        <li>Reservaciones</li>
                    </ul>
                </div>
                <div className="col">
                    <h3>Tipo de Negocio</h3>
                    <ul>
                        <li>Bar & Nightclub</li>
                        <li>Comedor Empresarial</li>
                        <li>Quickservice</li>
                        <li>Restaurante Alta Cocina</li>
                        <li>Restaurante Casual</li>
                    </ul>
                </div>
                <div className="col">
                    <h3>Soluciones</h3>
                    <ul>
                        <li>Administración</li>
                        <li>Cocina</li>
                        <li>Condor</li>
                        <li>Facturación</li>
                        <li>Hardware</li>
                        <li>Integraciones</li>
                        <li>Operación y Servicio</li>
                    </ul>
                </div>
                <div className="col">
                    <h3>Nosotros</h3>
                    <ul>
                        <li>¡Únete a nuestro equipo!</li>
                        <li>Parrot Software</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
)

Footer.propTypes = {
    className: p.string.isRequired
}

export { Footer }
