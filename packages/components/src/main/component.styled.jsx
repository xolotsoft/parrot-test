import styled from 'styled-components'

import { Main } from './component'

const MainStyled = styled(Main)`
    font-family: Arial, Helvetica, sans-serif;
    background-color: #ef4a4a;
    width: 100%;
    min-height: 400px;
    display: flex;
    margin-top: 120px;
    .container {
        max-width: 1140px;
        width: 100%;
        margin: 0 auto;
        padding: 32px 0;
        display: grid;
        grid-template-columns: 50% 50%;
        @media (max-width: 960px) {
            grid-template-columns: 100%;
            padding: 16px 0;
        }
        .col {
            display: flex;
            flex-direction: column;
            padding: 32px;
            align-items: end;
            @media (max-width: 960px) {
                padding: 16px;
            }
            h1 {
                color: #ffffff;
                text-align: left;
                font-size: 50px;
                font-weight: 500;
            }
            p {
                color: #fff;
                font-size: 20px;
                font-weight: 300;
                text-align: left;
            }
            button {
                background: #fff;
                padding: 8px 20px;
                border-radius: 2px;
                color: #ef4a4a;
                border: none;
                margin: 32px 0;
            }
            img {
                margin: auto;
                width: 100%;
                max-width: 500px;
            }
        }
    }
`

export { MainStyled as Main }
