import React from 'react'
import p from 'prop-types'

const Main = ({ className }) => (
    <section className={className}>
        <div className="container">
            <div className="col">
                <h1>El software para restaurantes de la era digital</h1>
                <p>
                    Migra tu sistema a Parrot y evoluciona a la nueva era de
                    tecnología para tu restaurante.
                </p>
                <button type="button">Más Información</button>
            </div>
            <div className="col">
                <img
                    src="https://parrotsoftware.io/parrotwp/wp-content/uploads/2019/10/monitor-e1580922642258.png"
                    alt=""
                />
            </div>
        </div>
    </section>
)

Main.propTypes = {
    className: p.string.isRequired
}

export { Main }
