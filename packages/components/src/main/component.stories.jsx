import React from 'react'

import { Main } from '.'

export default {
    title: 'Main',
    component: Main
}

const Template = args => <Main {...args} />

export const Primary = Template.bind({})
