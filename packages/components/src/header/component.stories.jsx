import React from 'react'

import { Header } from '.'

export default {
    title: 'Header',
    component: Header
}

const Template = args => <Header {...args} />

export const Main = Template.bind({})
