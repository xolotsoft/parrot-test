import styled from 'styled-components'

import { Header } from './component'

const HeaderStyled = styled(Header)`
    font-family: Arial, Helvetica, sans-serif;
    background-color: #ffffff;
    width: 100%;
    position: fixed;
    top: 0;
    left: 0;
    .header-bottom {
        background-color: #1d264d;
        color: #ffffff;
        .container {
            max-width: 1140px;
            width: 100%;
            display: flex;
            justify-content: space-around;
            margin: 0 auto;
            @media (max-width: 768px) {
                flex-direction: column;
                align-items: flex-start;
            }
            .header-logo {
                width: 120px;
                margin: 6px;
                min-height: 70px;
                position: relative;
                @media (max-width: 768px) {
                    width: 100%;
                    min-height: 25px;
                }
                img {
                    width: 120px;
                    position: absolute;
                    left: 16px;
                    top: 16px;
                }
                .menu-icon {
                    display: none;
                    @media (max-width: 768px) {
                        display: block;
                        right: 16px;
                        left: unset;
                        width: 40px;
                        outline: none;
                    }
                }
            }
            nav {
                @media (max-width: 960px) {
                    display: block;
                    align-self: center;
                }
                @media (max-width: 768px) {
                    align-self: flex-start;
                }
                padding: 0;
                ul {
                    padding: 0;
                    display: flex;
                    list-style: none;
                    transition: all 0.3s linear;
                    @media (max-width: 768px) {
                        margin-top: 50px;
                        flex-direction: column;
                        align-items: flex-start;
                        overflow: hidden;
                    }
                    li {
                        text-transform: uppercase;
                        padding: 8px 16px;
                        cursor: pointer;
                        font-size: 14px;
                    }
                }
            }
            button {
                margin-top: 12px;
                background-color: #3abb92;
                height: 40px;
                width: 140px;
                border: none;
                border-radius: 20px;
                color: #ffffff;
                cursor: pointer;
                @media (max-width: 768px) {
                    display: none;
                }
            }
        }
    }
`

export { HeaderStyled as Header }
