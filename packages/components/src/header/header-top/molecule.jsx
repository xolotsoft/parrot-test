import React from 'react'
import p from 'prop-types'

import { Actions } from './action'

const HeaderTop = ({ className }) => (
    <div className={className}>
        <div className="container">
            <Actions />
        </div>
    </div>
)

HeaderTop.propTypes = {
    className: p.string.isRequired
}

export { HeaderTop }
