import styled from 'styled-components'

import { HeaderTop } from './molecule'

const HeaderTopStyled = styled(HeaderTop)`
    font-family: Arial, Helvetica, sans-serif;
    background-color: #ffffff;
    color: #000000;
    .container {
        max-width: 1140px;
        width: 100%;
        margin: 0 auto;
        .header-top-actions {
            width: 100%;
            height: 42px;
            display: flex;
            justify-content: flex-end;
            align-items: flex-end;
            .action {
                display: flex;
                padding: 6px;
                cursor: pointer;
                .icon {
                    background: #ef4a4a;
                    padding: 0;
                    width: 30px;
                    height: 30px;
                    line-height: 30px;
                    border-radius: 30px;
                    text-align: center;
                    color: #ffffff;
                }
                p {
                    margin: 0 8px;
                    padding: 6px 0;
                    font-size: 14px;
                }
            }
        }
    }
`

export { HeaderTopStyled as HeaderTop }
