import React from 'react'
import p from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Action = ({ title, icon, action }) => (
    <div
        className="action"
        onClick={() => action()}
        role="button"
        tabIndex="0"
        onKeyPress={() => action()}
    >
        <div className="icon">
            <FontAwesomeIcon icon={icon} />
        </div>
        <p>{title}</p>
    </div>
)

Action.propTypes = {
    title: p.string,
    icon: p.string,
    action: p.func
}

Action.defaultProps = {
    title: 'Acción',
    icon: 'user',
    action: null
}

export { Action }
