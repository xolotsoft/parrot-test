import React from 'react'
import p from 'prop-types'
import _ from 'lodash'

import { Action } from './action'

const Actions = ({ actions }) => (
    <div className="header-top-actions">
        {actions.map(e => (
            <Action
                title={e.title}
                icon={e.icon}
                action={e.action}
                key={_.uniqueId('heder-action')}
            />
        ))}
    </div>
)

Actions.propTypes = {
    actions: p.arrayOf(
        p.shape({
            title: p.string,
            icon: p.string,
            action: p.func
        })
    )
}

Actions.defaultProps = {
    actions: [
        { title: '(81) 2046 6363', icon: 'phone-volume', action: null },
        {
            title: 'Iniciar Sesión',
            icon: 'user',
            action: () => {
                window.location.href = 'http://localhost:8010/'
            }
        },
        { title: '', icon: 'search', action: null }
    ]
}

export { Actions }
