import React from 'react'

import { HeaderTop } from '.'

export default {
    title: 'Header/Molecules/HeaderTop',
    component: HeaderTop
}

const Template = args => <HeaderTop {...args} />

export const Primary = Template.bind({})
