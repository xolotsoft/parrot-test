/* eslint-disable jsx-a11y/no-noninteractive-element-to-interactive-role */
import React from 'react'
import p from 'prop-types'
import { library } from '@fortawesome/fontawesome-svg-core'
import {
    faUser,
    faPhoneVolume,
    faSearch
} from '@fortawesome/free-solid-svg-icons'

import { HeaderTop } from './header-top'

library.add(faUser, faPhoneVolume, faSearch)

const Header = ({ className }) => {
    const [toggle, setToggle] = React.useState(false)
    return (
        <header className={className}>
            <HeaderTop />
            <div className="header-bottom">
                <div className="container">
                    <div className="header-logo">
                        <img
                            src="https://parrotsoftware.io/parrotwp/wp-content/uploads/2019/10/cropped-logo.png"
                            alt="PArrot"
                        />
                        <img
                            src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Hamburger_icon_white.svg/1024px-Hamburger_icon_white.svg.png"
                            alt="parrot-menu"
                            className="menu-icon"
                            role="button"
                            tabIndex="0"
                            onKeyPress={() => setToggle(!toggle)}
                            onClick={() => setToggle(!toggle)}
                        />
                    </div>
                    <nav>
                        <ul style={{ height: toggle ? '160px' : '0px' }}>
                            <li>software</li>
                            <li>tipos de negocio</li>
                            <li>soluciones</li>
                            <li>nosotros</li>
                            <li>contacto</li>
                        </ul>
                    </nav>
                    <button type="button">Solicita un Demo</button>
                </div>
            </div>
        </header>
    )
}

Header.propTypes = {
    className: p.string.isRequired
}

export { Header }
