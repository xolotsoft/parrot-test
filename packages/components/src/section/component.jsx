import React from 'react'
import p from 'prop-types'

const Section = ({ className }) => (
    <div className={className}>
        <div className="container">
            <h2>
                Conectamos la tecnología <br /> restaurantera en una solución.
            </h2>
            <img
                src="https://parrotsoftware.io/parrotwp/wp-content/uploads/2020/07/HARDWARE-02-copy-scaled.jpg"
                alt="Hardware"
            />
            <div className="solutions-container">
                <div className="col">
                    <h3>Solución en la nube</h3>
                    <p>
                        Controla y visualiza todas las operaciones de tus
                        restaurantes desde una plataforma.
                    </p>
                </div>
                <div className="col">
                    <h3>Ecosistema de soluciones</h3>
                    <p>
                        Te conectamos con las aplicaciones más populares del
                        mercado para una solución &quot;Todo en Uno&quot;.
                    </p>
                </div>
                <div className="col">
                    <h3>Incrementa tus ventas</h3>
                    <p>
                        Nos esforzamos día con día para que puedas vender más y
                        mejorar tu rentabilidad.
                    </p>
                </div>
            </div>
        </div>
    </div>
)

Section.propTypes = {
    className: p.string.isRequired
}

export { Section }
