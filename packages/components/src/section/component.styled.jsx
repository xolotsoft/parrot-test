import styled from 'styled-components'

import { Section } from './component'

const SectionStyled = styled(Section)`
    font-family: Arial, Helvetica, sans-serif;
    background: #ffffff;
    width: 100%;
    min-height: 400px;
    display: flex;
    padding-bottom: 80px;
    .container {
        display: flex;
        flex-direction: column;
        max-width: 1140px;
        width: 100%;
        margin: 0 auto;
        align-items: center;
        h2 {
            font-size: 50px;
            color: #1d264c;
            text-align: center;
        }
        img {
            max-width: 800px;
            width: 100%;
        }
        .solutions-container {
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
            .col {
                padding: 16px;
                max-width: 320px;
                h3 {
                    font-size: 22px;
                    color: #1d264c;
                    margin-bottom: 16px;
                }
                p {
                    color: #2c2862;
                    font-size: 20px;
                    font-weight: 300;
                    margin-top: 0;
                }
            }
        }
    }
`

export { SectionStyled as Section }
